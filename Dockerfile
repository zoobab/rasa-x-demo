FROM rasa/rasa-sdk:3.2.2
COPY main.py .
COPY models/model.tar.gz /app/models
CMD ["run", "python", "/app/main.py"]
